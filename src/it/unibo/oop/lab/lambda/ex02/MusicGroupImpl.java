package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    /*	Set<String> list = new HashSet<>();
    	this.songs.forEach(t -> {
    		list.add(t.getSongName());
	    });
	    Stream<String> stream = list.stream().sorted();
	    return stream; */
    	return null;
    }

    @Override
    public Stream<String> albumNames() {
    	/*Set<String> list = new HashSet<>();
	    this.songs.forEach(t -> {
	    	if (t.getAlbumName().isPresent()) {
	    		list.add(t.getAlbumName().get());
	    	}
	   	});
	   	Stream<String> stream = list.stream(); 
	   	return stream;*/
    	return null;
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    	/*Set<String> list = new HashSet<>();
    	this.albums.forEach((s, i) -> {
    		if (i.intValue() == year) {
    			list.add(s);
    		}
    	});
    	Stream<String> stream = list.stream(); 
	   	return stream;*/
    	return null;
    }

    @Override
    public int countSongs(final String albumName) {
        /*int abc = 0;
        this.songs.forEach(t -> {
        	if (t.getAlbumName().isPresent() && t.getAlbumName().get() == albumName) {
        		abc = abc + 1;
        	}
        });
    	return abc;*/
    	return -1;
    }

    @Override
    public int countSongsInNoAlbum() {
    	/*int abc = 0;
        this.songs.forEach(t -> {
        	if (t.getAlbumName().isEmpty()) {
        		abc = abc + 1;
        	}
        });
    	return abc;*/
        return -1;
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return null;
    }

    @Override
    public Optional<String> longestSong() {
        return null;
    }

    @Override
    public Optional<String> longestAlbum() {
        return null;
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
